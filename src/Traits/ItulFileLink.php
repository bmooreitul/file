<?php
	
	namespace App\Traits;

	trait ItulFileLink {

		public function attachItulFile(\App\Models\ItulFile $file){
			$file->attachTo($this);
			return $this;
		}

		public function itulFiles(){
	        return $this->belongsToMany(\App\Models\ItulFile::class, 'itul_file_maps', 'attachable_id', 'itul_file_id')->where('attachable_type', static::class);
	    }
	}