<table class="table">
    <tbody>
        <tr>
            <td>Name</td>
            <td><a href="{{$file->view_url}}">{{$file->icon}} {{$file->display_name}}</a></td>
        </tr>
        <tr>
            <td>Type</td>
            <td>{{$file->type}}</td>
        </tr>
        <tr>
            <td>Size</td>
            <td>{{$file->humanSize}}</td>
        </tr>
        <tr>
            <td>Created</td>
            <td>{{$file->created_at->format('m/d/Y g:i a')}}</td>
        </tr>
        <tr>
            <td>Updated</td>
            <td>{{$file->updated_at->format('m/d/Y g:i a')}}</td>
        </tr>
    </tbody>
</table>