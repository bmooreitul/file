<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItulFileMap extends Model{
    
    use HasFactory;

    protected $fillable = [
        'itul_file_id',
        'attachable_id',
        'attachable_type',
    ];

    public function file(){
        return $this->belongsTo('\App\Models\ItulFile');
    }

    public function attachable(){
        return $this->morphTo();
    }
}