<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItulFile extends Model{
	use HasFactory;

	protected $fillable = [
		'display_name',
		'hash_name',
		'path',
		'created_by',
		'type',
		'mime',
		'size',
		'description',
		'view_url',
		'meta_data',
	];

	protected $appends = [
		'public_view_url',
		'icon',
	];

	protected $casts = [
		'meta_data' => 'object',
	];

	private $_metaDataWrapper;
	

	//HANDLE BOOT METHODS
	protected static function booted(){

		//LISTEN FOR DELETING A FILE
		static::deleting(function($fileModel){       
			unlink($fileModel->fullPath);     
			if($maps = $fileModel->fileMaps) foreach($maps as $map) $map->delete();
		});

		//AFTER A FILE IS CREATED
		static::created(function($fileModel){
			$fileModel->view_url = route('itul.file.view', $fileModel->hash_name);
			$fileModel->saveQuietly();
		});

		static::retrieved(function($fileModel){
			$fileModel->_metaDataWrapper = $fileModel->getMetaAttribute();
		});

		static::saving(function($fileModel){
			$fileModel->meta_data = json_encode($fileModel->_metaDataWrapper);
		});

		static::saved(function($fileModel){
			$fileModel->_resetMetaWrapper();
		});
	}

	//FILE MAPS RELATIONSHIP
	public function fileMaps(){
		return $this->hasMany(\App\Models\ItulFileMap::class, 'itul_file_id', 'id');
	}

	//ATTACH THE FILE TO AN OBJECT
	public function attachTo($object){

		if(is_object($object) && $object instanceof \Illuminate\Database\Eloquent\Model && $object->exists){
			(new \App\Models\ItulFileMap)->fill([
				'itul_file_id'      => $this->id,
				'attachable_type'   => get_class($object),
				'attachable_id'     => $object->id,
			])->save();
		}        

		return $this;
	}

	public static function mimeFallback($filename){

        $mime_types = \CheckMimeType::all();

		//CHECK IF THE EXTENSION IS KNOWN
		if($ext = pathinfo(trim(strtolower($filename)), PATHINFO_EXTENSION)) if(array_key_exists($ext, $mime_types)) return $mime_types[$ext];

        //CHECK IF THE MIME TYPE CAN BE PARSED
        if(function_exists('finfo_open')) {
            $finfo 		= finfo_open(FILEINFO_MIME);
            $mimetype 	= finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }

        //DEFAULT TO OCTET
        return 'application/octet-stream';
	}

	public function raw(){
		@readfile($this->fullPath);
	}

	public function attachToMessage(\Illuminate\Mail\Message $message){

		//ATTACH THE THE FILE
		$message->attach($this->fullPath, ['as' => $this->display_name, 'mime' => $this->mime]);

		return $this;
	}

	public function getPublicViewUrlAttribute(){
		return route('itul.file.view.public', $this->hash_name);
	}

	public static function create($temp_path, $name = null){

		//SETUP DEFAULTS
		if(is_object($temp_path) && (get_class($temp_path) == 'Illuminate\Http\UploadedFile' || get_class($temp_path) == 'Symfony\Component\HttpFoundation\File\UploadedFile')){
			$ext        = $temp_path->getClientOriginalExtension();
			$ext        = strlen($ext) ? $ext : '';
			$name       = is_null($name) ? $temp_path->getClientOriginalName() : $name;
			$mime       = $temp_path->getMimeType();
			$size       = $temp_path->getSize();
			$temp_path  = $temp_path->getRealPath();
		}
		else{
			$ext        = is_null($name) ? pathinfo($temp_path, PATHINFO_EXTENSION) : pathinfo($name, PATHINFO_EXTENSION);
			$name       = is_null($name) ? basename($temp_path) : $name;
			$mime       = !function_exists('mime_content_type') ? self::mimeFallback($temp_path) : mime_content_type($temp_path);
			$size       = filesize($temp_path);
		}

		$user           = \Auth::user();
		$user_id        = $user ? $user->id : null;
		$hash           = md5(microtime(true).rand(0,9999));
		$path           = storage_path().'/files/';

		//CREATE THE PATH IF NECESSARY
		if(!file_exists($path)) mkdir($path, 0755, true);

		//MOVE THE FILE
		rename($temp_path, $path.$hash);

		//INSTANTIATE THE FILE
		$file = (new \App\Models\ItulFile)->fill([
			'display_name'  => $name,
			'hash_name'     => $hash,
			'path'          => $path,
			'type'          => ltrim($ext, '.'),
			'mime'          => $mime,
			'size'          => $size,
		]);

		//SAVE THE FILE
		$file->save();
		$file->fresh();

		//SEND THE FILE MODEL BACK
		return $file;
	}

	public function getHumanSizeAttribute() {
		$size = $this->size;
		$bytes = array('B','KB','MB','GB','TB');
		foreach($bytes as $val) {
			if($size > 1024){
				$size = $size / 1024;
			}else{
				break;
			}
		}
		return round($size, 2)." ".$val;
	}

	public function getFullPathAttribute(){

		return $this->path.$this->hash_name;
	}

	public function getPath(){
		return route('itul.file.view', $this->hash_name);
	}

	public function tempCopy(){

		//DEFINE A TEMP FILE PATH
		$target = sys_get_temp_dir().'/'.$this->hash_name.'.'.$this->type;

		//IF THE TEMP FILE WAS CREATED SEND BACK THE PATH
		if(file_put_contents($target, file_get_contents($this->fullPath))) return $target;

		//DEFAULT TO FALSE
		return false;
	}

	public function duplicate(){
		return \App\ItulFile::create($this->tempCopy, $this->display_name);
	}

	public function copyToDirectory($dir){

		if(!file_exists($dir)) mkdir($dir, 0755, true);
		chmod($dir, 0755);

		$target = rtrim($dir, '/').'/'.$this->hash_name.'.'.$this->type;

		copy($this->fullPath, $target);

		return $target;
	}
	
	public function icon(){

		$default 	= '<i class="far fa-file file-extension-icon"></i>';
		$type 		= ltrim(strtolower($this->type), '.');
		$types 		= [
			'pdf'   => '<i class="far fa-file-pdf file-extension-icon"></i>',
			'docx'  => '<i class="far fa-file-word file-extension-icon"></i>',
			'xlsx'  => '<i class="far fa-file-excel file-extension-icon"></i>',
			'ppt'   => '<i class="far fa-file-powerpoint file-extension-icon"></i>',
			'png'   => '<i class="far fa-file-image file-extension-icon"></i>',
			'jpeg'  => '<i class="far fa-file-image file-extension-icon"></i>',
			'jpg'   => '<i class="far fa-file-image file-extension-icon"></i>',
			'gif'   => '<i class="far fa-file-image file-extension-icon"></i>',
			'zip'   => '<i class="far fa-file-archive file-extension-icon"></i>',
		];

		if(array_key_exists($type, $types)) return $types[$type];

		return $default;
	}

	public function getIconAttribute(){
		return $this->icon();
	}

	public function getUrlAttribute(){
		return route('itul.file.view', $this->hash_name);
	}

	private function _viewImage($delete = false){
		header("Content-Type: {$this->mime}");

		@readfile($this->fullPath);
		if($delete){
			unlink($this->fullPath);
			$this->delete();
		}
		exit;
	}

	//VIEW THE FILE
	public function view($delete = false){

		$mime = strtolower(trim($this->mime));
		if(substr($mime, 0, 6) == 'image/') return $this->_viewImage($delete);

		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false); // required for certain browsers
		header("Content-Disposition: attachment; filename=\"".$this->display_name."\";" );
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ". @filesize($this->fullPath));
		header( "Content-Description: File Transfer");
		header("Content-Type: {$this->mime}");
		@readfile($this->fullPath);

		if($delete){
			unlink($this->fullPath);
			$this->delete();
		}
		exit;
	}


//--------------------------------------------------// META DATA METHODS //--------------------------------------//

	//RECURSIVELY PARSE THE META DATA INTO A GENERAL OBJECT
	public static function _parseMetaDataObjects($data, $object){
		foreach($data as $k => $v){
			if(is_array($v) || (is_object($v) && get_class($v) == 'stdClass')){
				$object->$k = new class((array)$v) extends \GeneralObject {
					public function __construct($data = []){
						parent::__construct($data);
						\App\Models\ItulFile::_parseMetaDataObjects($data, $this);
					}
				};
			}
		}
		return $object;
	}

	//RESET THE META WRAPPER
	public function _resetMetaWrapper(){
		$this->_metaDataWrapper = null;
		$this->getMetaAttribute();
		return $this;
	}

	//CREATE A DYNAMIC CLASS FOR THE META DATA 
	private function _metaDataFactory($value){
		$this->_metaDataWrapper = new class((array)$value) extends \GeneralObject {
			public function __construct($data = []){
				parent::__construct($data);
				\App\Models\ItulFile::_parseMetaDataObjects($data, $this);
			}
		};
		return $this->_metaDataWrapper;
	}

	//MUTATE THE ->meta CALL TO PARSE CORRECTLY
	public function getMetaAttribute(){		
		if(is_null($this->_metaDataWrapper)) $this->_metaDataFactory($this->meta_data);		
		return $this->_metaDataWrapper;
	}

	//MUTATE THE ->meta*= CALL TO SET CORRECTLY
	public function setMetaAttribute($value = []){
		$this->_metaDataFactory($value);
	}

	//MUTATE THE META DATA ATTRIBUTE
	public function setMetaDataAttribute($value){
		$this->attributes['meta_data'] = $value;
	}
}
