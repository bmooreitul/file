<?php

namespace Itul\File;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Artisan;

class FileServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'file');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'file');
        //$this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes/routes.php');

        //MAKE SURE WE ARE RUNNING IN THE CONSOLE
        if($this->app->runningInConsole()){ 
                
            //DEFINE THE PUBLISHABLE FILES
            $this->publishes([
                __DIR__.'/config/config.php'                    => config_path('Itulfile.php'),
                __DIR__.'/Models/ItulFile.php'                  => app_path('Models/ItulFile.php'),
                __DIR__.'/Models/ItulFileMap.php'               => app_path('Models/ItulFileMap.php'),
                __DIR__.'/Controllers/ItulFilesController.php'  => app_path('Http/Controllers/ItulFilesController.php'),
                __DIR__.'/Traits/ItulFileLink.php'              => app_path('Traits/ItulFileLink.php'),
                __DIR__.'/resources/views/info.blade.php'       => base_path('resources/views/vendor/itul/file/info.blade.php'),
            ], 'itul-file-assets');

            //AUTO PUBLISH THE ASSETS
            Artisan::call('vendor:publish', ['--tag' => 'itul-file-assets']);

            //RUN MIGRATIONS FOR THIS PACKAGE ONLY AS NEEDED
            foreach(glob(__DIR__.'/database/migrations/*.php') as $migrationFile) Artisan::call("migrate", ['--path' => str_replace(base_path(), '', $migrationFile)]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'file');

        // Register the main class to use with the facade
        $this->app->singleton('itulFile', function () {
            return new File;
        });
    }
}
