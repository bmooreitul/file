<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ItulFilesController extends Controller {

    //DOWNLOAD A FILE FROM A HASH NAME
    public function view($hash_name, $temp = false){

        //TRY TO FIND THE FILE BY HASH
        if($file = \App\Models\ItulFile::where("hash_name", $hash_name)->first()) return $file->view(boolval($temp));

        //FILE WAS NOT FOUND
        abort(404);
    }

    //CREATE FILE MODELS FROM UPLOAD
    public function upload(Request $request){

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');

        $targetObj = false;
        $input = $request->all();
        if(isset($input['attachable_type']) && isset($input['attachable_id'])){
            $modelName = '\\'.ltrim($input['attachable_type'], '\\');
            try{

                if($targetObj = $modelName::find($input['attachable_id'])){
                    if(!$targetObj->exists) $targetObj = false;
                }
                else{
                    $targetObj = false;
                }
            }
            catch(\Exception $e){
                $targetObj = false;
                //SILENT
            }
        }
        //DEFAULT RESPONSE
        $res = [
            'files' => [],
            'error' => [],
        ];

        //LOOP THROUGH THE FILES AND SET RESULTS/ERRORS FROM STORAGE SUCCESS
        if($files = $request->file()) foreach($files as $file){
            if($file->isValid()){
                $f = \App\Models\ItulFile::create($file);
                if($targetObj){
                    $map = (new \App\Models\ItulFileMap)->fill([
                        'file_id'           => $f->id,
                        'attachable_type'   => $input['attachable_type'],
                        'attachable_id'     => $input['attachable_id'],
                    ])->save();
                }
                $res['files'][] = $f;
            }
            else{
                $res['error'][] = $file->getClientOriginalName();
            }
        }

        //SEND BACK THE RESULTS
        return response()->json($res);
    }

    public function trash($hash_name){

        //TRY TO FIND THE FILE BY HASH
        if($file = \App\Models\ItulFile::where("hash_name", $hash_name)->first()){
            $file->delete();
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    public function info(Request $request, $hash_name){

        //RENDER THE INFORMATION VIEW IF THE FILE WAS LOADED
        if($file = \App\Models\ItulFile::where("hash_name", $hash_name)->first()) return \View::make('vendor.itul.files.info', compact('file'))->render();

        //RENDER THE NO INFORMATION LOADED VIEW
        return \View::make('vendor.itul.files.info-none');
    }
}
