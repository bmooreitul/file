<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItulfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('itul_files')){
            Schema::create('itul_files', function (Blueprint $table) {
                $table->id();
                $table->string('display_name');
                $table->string('hash_name');
                $table->string('path');
                $table->bigInteger('created_by')->nullable();
                $table->string('type');
                $table->string('mime');
                $table->bigInteger('size')->default('0');
                $table->longText('description')->nullable();
                $table->longText('view_url')->nullable();
                $table->timestamps();
            });

            \DB::statement('ALTER TABLE `itul_files` ADD COLUMN meta_data JSON NOT NULL DEFAULT (\'{}\')');
        }

        if(!Schema::hasTable('itul_file_maps')){
            Schema::create('itul_file_maps', function (Blueprint $table) {
                $table->id();
                $table->string('itul_file_id');
                $table->morphs('attachable');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itul_files');
        Schema::dropIfExists('itul_file_maps');
    }
}
