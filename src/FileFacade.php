<?php

namespace Itul\File;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\File\Skeleton\SkeletonClass
 */
class FileFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Models\ItulFile::class;
    }
}
