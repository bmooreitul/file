<?php

use Illuminate\Support\Facades\Route;

Route::get('/ifile/v/{hash}', [\App\Http\Controllers\FilesController::class, 'view'])->name('itul.file.view.public');

Route::group(['middleware' => 'auth'], function () {

	Route::get('/ifiles/view/{hash}/{temp?}', [\App\Http\Controllers\FilesController::class, 'view'])->name('itul.file.view');
    Route::post('/ifiles/upload', [\App\Http\Controllers\FilesController::class, 'upload'])->name('itul.file.upload');
    Route::get('/ifiles/info/{hash}', [\App\Http\Controllers\FilesController::class, 'info'])->name('itul.file.info.get');
	Route::post('/ifiles/trash/{hash}',[App\Http\Controllers\ItulFilesController::class, 'trash'])->name('itul.file.trash');
});